% Name: 
%      script_figure4
%
% Purpose:
%      This figure shows computed error measures (ME, MAE, RMSE) 
%      for different background solar wind models (ESWF, WSA, PS27). 
%      The red dashed-dotted lines indicate the ICME shock arrival, 
%      and the blue and black dashed lines indicate the mean start 
%      and end time of the magnetic cloud (MC) structure. The 
%      events are grouped into four ICME impact speed categories 
%      where blue lines include all recorded ICMEs. The baseline for 
%      each panel given as blue dotted horizontal line is calculated 
%      outside recorded ICME disturbances.
%
% Reference:
%      Preconditioning of interplanetary space due to transient CME 
%      disturbances
%             
% Author:
%      Martin A. Reiss, Nov 2016.
%%
clear
f = figure;
load('../data/measurements.mat')                 %Load data files
load('../data/predictions.mat')
load('../data/icme.mat')
tMeas = tACE;                                   
vMeas = vACE;

dt        = 0.25;
dateStart = '01-01-2011';                        %Select start date
dateEnd   = '01-12-2015';                        %Select end date
format    = 'dd-mm-yyyy';

l1 = -10;
l2 = 40;

%% Panel 1: ME
measure = 1;
y1 = -25;
y2 = 200;

subplot(3,3,1)
tPred = tESWF;
vPred = vESWF;
ploterrormeasure
ylim([y1,y2])
set(gca, 'XTick',(3:4:51)-11,'XTickLabel', -2:1:10);
leg = legend('v > 300 km/s (N = 136)', 'v > 350 km/s (N = 122)','v > 400 km/s (N = 78)',...
    'v > 450 km/s (N = 46)', 'baseline', 'shock arrival','MC start','MC end',...
    'Location','northeast', 'Position',[217 305 0.2 0.2],'Orientation','horizontal');
set(leg,'FontSize',8);
xlim([l1,l2])
ylabel('ME [km/s]') 
title('ESWF')
box on

load('../data/measurements.mat')              
load('../data/predictions.mat')
load('../data/icme.mat')

subplot(3,3,2)
tPred = tWSA;
vPred = vWSA;
ploterrormeasure
ylim([y1,y2])
set(gca, 'XTick',(3:4:51)-11,'XTickLabel', -2:1:10);
xlim([l1,l2])
ylabel('ME [km/s]') 
title('WSA')
legend off
box on

load('../data/measurements.mat')             
load('../data/predictions.mat')
load('../data/icme.mat')

subplot(3,3,3)
tPred = tRec;
vPred = vRec;
ploterrormeasure
ylim([y1,y2])
ylabel('ME [km/s]') 
title('PS27')
set(gca, 'XTick',(3:4:51)-11,'XTickLabel', -2:1:10);
xlim([l1,l2])
box on

%% Panel 2: MAE
measure = 2;
y1 = 60;
y2 = 200;

subplot(3,3,4)
tPred = tESWF;
vPred = vESWF;
ploterrormeasure
ylim([y1,y2])
ylabel('MAE [km/s]')
set(gca, 'XTick',(3:4:51)-11,'XTickLabel', -2:1:10);
xlim([l1,l2])
box on

load('../data/measurements.mat')           
load('../data/predictions.mat')
load('../data/icme.mat')

subplot(3,3,5)
tPred = tWSA;
vPred = vWSA;
ploterrormeasure
ylim([y1,y2])
ylabel('MAE [km/s]')
set(gca, 'XTick',(3:4:51)-11,'XTickLabel', -2:1:10);
xlim([l1,l2])
legend off
box on

load('../data/measurements.mat')     
load('../data/predictions.mat')
load('../data/icme.mat')

subplot(3,3,6)
tPred = tRec;
vPred = vRec;
ploterrormeasure
ylim([y1,y2])
ylabel('MAE [km/s]')
set(gca, 'XTick',(3:4:51)-11,'XTickLabel', -2:1:10);
xlim([l1,l2])
legend off
box on

%% Panel 3: RMSE
measure = 3;
y1 = 80;
y2 = 200;

subplot(3,3,7)
tPred = tESWF;
vPred = vESWF;
ploterrormeasure
ylim([y1,y2])
ylabel('RMSE [km/s]')
set(gca, 'XTick',(3:4:51)-11,'XTickLabel', -2:1:10);
xlim([l1,l2])
box on

load('../data/measurements.mat')  
load('../data/predictions.mat')
load('../data/icme.mat')

subplot(3,3,8)
tPred = tWSA;
vPred = vWSA;
ploterrormeasure
ylim([y1,y2])
ylabel('RMSE [km/s]')
set(gca, 'XTick',(3:4:51)-11,'XTickLabel', -2:1:10);
xlim([l1,l2])
legend off
box on

load('../data/measurements.mat')    
load('../data/predictions.mat')
load('../data/icme.mat')

subplot(3,3,9)
tPred = tRec;
vPred = vRec;
ploterrormeasure
ylim([y1,y2])
set(gca, 'XTick',(3:4:51)-11,'XTickLabel', -2:1:10);
xlim([l1,l2])
ylabel('RMSE [km/s]')
legend off
box on

%%
set(f, 'Position',[0,0,1200,1000])
set(gcf,'Units','inches');
screenposition = get(gcf,'Position');
set(gcf,...
    'PaperPosition',[0 0 screenposition(3:4)],...
    'PaperSize',[screenposition(3:4)]);
print -dpdf -painters ../figures/Figure4