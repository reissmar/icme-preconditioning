function [ errorMeasure ] = geterroranalysis( vPred, vMeas )
%GETERRORANALYSIS computes error measures
%
% Purpose:
%      This function computes standard point-by-point error measures.
%
% Arguments: (Input)
%      vPred           - Predicted speed time-line
%      vMeas           - Measured speed time-line
%
% Arguments: (Output)
%      errorMeasure    - Basic error measures
%      errorMeasure(1) - Mean Error (ME)
%      errorMeasure(2) - Mean Absolute Error (MAE)
%      errorMeasure(3) - Root Mean Square Error (RMSE)
%      errorMeasure(4) - Mean Measurement
%      errorMeasure(5) - Standard Deviation Measurement
%      errorMeasure(6) - Mean Prediction
%      errorMeasure(7) - Standard Deviation Prediction
%
% Reference:
%      Verification of high-speed solar wind stream forecasts using
%      operational solar wind models.
%
% Author:
%      Martin A. Reiss, Oct 2015.

ME   = mean(vMeas)-mean(vPred);                  %Compute error measures      
MAE  = sum(abs(vMeas(:)-vPred(:)))/numel(vPred);
RMSE = sqrt(sum((vMeas(:)-vPred(:)).^2)/numel(vPred));

meanMeas   = mean(vMeas);
stddevMeas = std(vMeas);
meanPred   = mean(vPred);
stddevPred = std(vPred);

errorMeasure(1) = ME;                            %Prepare output
errorMeasure(2) = MAE;
errorMeasure(3) = RMSE;
errorMeasure(4) = meanMeas;
errorMeasure(5) = stddevMeas;
errorMeasure(6) = meanPred;
errorMeasure(7) = stddevPred;