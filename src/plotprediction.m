function [fig] = plotprediction(t, y, startDateStr, endDateStr)
%PLOTPREDICTION returns a plot with date time grid
%
% Purpose:
%      This function plots the input variables t,y on specified time grid. 
%
% Arguments: (Input)
%      t, y            - input time-line
%      startDateStr    - required start date
%      endDateStr      - required end date
%
% Arguments: (Output)
%      fig             - prepared figure
%
% Author:
%      Martin A. Reiss, Oct 2015.

startDate = datenum(startDateStr, 'dd-mm-yyyy');
endDate   = datenum(endDateStr, 'dd-mm-yyyy');

fig = plot(t, y); 

day = 0;                                         %Compute tick locations 
for year = 1975:2050                             
dayTemp = datenum(year,1:12,ones(1,12));
day = [day dayTemp];
end

set(gca, 'XTick', day);                          %Update tick locations
datetick('x', 'dd-mmm-yy', 'keepticks')          %Update tick text
axis([startDate endDate 250 inf])                %Set axis range