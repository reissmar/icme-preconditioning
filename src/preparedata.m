% DATA PREPARATION
load('../data/predictions.mat')

tPred  = tESWF;
vPred  = vESWF;
dt     = 0.25;

cond   = (tPred > datenum(dateStart,format)...   %Get predicted speed for 
    & tPred < datenum(dateEnd,format));          %selected time period.
tPred  = tPred(cond);
vPred  = vPred(cond);
cond   = (tMeas > datenum(dateStart,format)...   %Get observed speed for
    & tMeas < datenum(dateEnd,format));          %selected time period.
tMeas  = tMeas(cond);
vMeas  = vMeas(cond);

tStart = datenum(dateStart,format)+1;            %Prepare 6h time grid
tEnd   = datenum(dateEnd,format)-1;
tGrid  = tStart:dt:tEnd;
vMeas  = interp1(tMeas,vMeas,tGrid)';
vPred  = interp1(tPred,vPred,tGrid)';
tPred  = tGrid;
tMeas  = tGrid;

tPredESWF = tPred;
vPredESWF = vPred;
vMeasESWF = vMeas;

%%
load('../data/predictions.mat')

tPred  = tWSA;
vPred  = vWSA;
dt     = 0.25;

cond   = (tPred > datenum(dateStart,format)...   %Get predicted speed for 
    & tPred < datenum(dateEnd,format));          %selected time period.
tPred  = tPred(cond);
vPred  = vPred(cond);
cond   = (tMeas > datenum(dateStart,format)...   %Get observed speed for
    & tMeas < datenum(dateEnd,format));          %selected time period.
tMeas  = tMeas(cond);
vMeas  = vMeas(cond);

tStart = datenum(dateStart,format)+1;            %Prepare 6h time grid
tEnd   = datenum(dateEnd,format)-1;
tGrid  = tStart:dt:tEnd;
vMeas  = interp1(tMeas,vMeas,tGrid)';
vPred  = interp1(tPred,vPred,tGrid)';
tPred  = tGrid;
tMeas  = tGrid;

tPredWSA = tPred;
vPredWSA = vPred;
vMeasWSA = vMeas;
%%
load('../data/predictions.mat')

tPred  = tRec;
vPred  = vRec;
dt     = 0.25;

cond   = (tPred > datenum(dateStart,format)...   %Get predicted speed for 
    & tPred < datenum(dateEnd,format));          %selected time period.
tPred  = tPred(cond);
vPred  = vPred(cond);
cond   = (tMeas > datenum(dateStart,format)...   %Get observed speed for
    & tMeas < datenum(dateEnd,format));          %selected time period.
tMeas  = tMeas(cond);
vMeas  = vMeas(cond);

tStart = datenum(dateStart,format)+1;            %Prepare 6h time grid
tEnd   = datenum(dateEnd,format)-1;
tGrid  = tStart:dt:tEnd;
vMeas  = interp1(tMeas,vMeas,tGrid)';
vPred  = interp1(tPred,vPred,tGrid)';
tPred  = tGrid;
tMeas  = tGrid;

tPredRec = tPred;
vPredRec = vPred;
vMeasRec = vMeas;