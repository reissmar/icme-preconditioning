function [ m ] = getintervalerror( tPred, vPred, dateStart, dateEnd, dt)

load('../data/icme.mat')
load('../data/measurements.mat')              
tMeas = tACE;                                  
vMeas = vACE; 
format = 'dd-mm-yyyy';                             

cond = (tCMEstart > datenum(dateStart,format)...   
    & tCMEstart < datenum(dateEnd,format));          

tCMEstart = tCMEstart(cond);
tCMEend = tCMEend(cond);

cond   = (tPred > datenum(dateStart,format)...   %Get predicted speed for 
    & tPred < datenum(dateEnd,format));          %selected time period.
tPred  = tPred(cond);
vPred  = vPred(cond);
cond   = (tMeas > datenum(dateStart,format)...   %Get observed speed for
    & tMeas < datenum(dateEnd,format));          %selected time period.
tMeas  = tMeas(cond);
vMeas  = vMeas(cond);

tStart = datenum(dateStart,format)+1;            %Prepare 6h time grid
tEnd   = datenum(dateEnd,format)-1;
tGrid  = tStart:dt:tEnd;
vMeas  = interp1(tMeas,vMeas,tGrid)';
vPred  = interp1(tPred,vPred,tGrid)';
tPred  = tGrid;
tMeas  = tGrid;

%%
idx = 1;
idx1 = 1;
idx2 = 1;
idx3 = 1;
for i=1:numel(tMeas)
    for j=1:numel(tCMEstart)
        if tMeas(i) >= tCMEstart(j) && tMeas(i) <= tCMEend(j)
        tMeasIn(idx) = tMeas(i);
        vMeasIn(idx) = vMeas(i);
        vPredIn(idx) = vPred(i);
        vDiffIn(idx) = vMeas(i) - vPred(i);
        idx = idx + 1; 
        elseif tMeas(i) >= (tCMEstart(j)-2) && tMeas(i) <= tCMEstart(j)
        vMeasBefore(idx2) = vMeas(i);
        vPredBefore(idx2) = vPred(i);
        vDiffBefore(idx2) = abs(vMeas(i) - vPred(i));    
        vDiffBefore(idx2) = vMeas(i) - vPred(i);    
        idx2 = idx2 + 1;
        elseif tMeas(i) >= tCMEend(j) && tMeas(i) <= tCMEend(j) +(tCMEend(j)-tCMEstart(j))
        vMeasAfter(idx3) = vMeas(i);
        vPredAfter(idx3) = vPred(i);
        vDiffAfter(idx3) = abs(vMeas(i) - vPred(i));    
        vDiffAfter(idx3) = vMeas(i) - vPred(i);    
        idx3 = idx3 + 1; 
        else
        tMeasOut(idx1) = tMeas(i);
        vMeasOut(idx1) = vMeas(i);
        vPredOut(idx1) = vPred(i);
        vDiffOut(idx1) = vMeas(i) - vPred(i);
        idx1 = idx1 + 1;
        end    
    end
end

[ errorMeasureIn ] = geterroranalysis( vPredIn, vMeasIn );
[ errorMeasureAfter] = geterroranalysis( vPredAfter, vMeasAfter );
[ errorMeasureOut] = geterroranalysis( vPredOut, vMeasOut );
m = [errorMeasureIn',errorMeasureAfter',errorMeasureOut'];