% Name: 
%      script_figure2
%
% Purpose:
%      This figure shows the differences between measured and 
%      modeled (left to right: ESWF, WSA, PS27) solar wind speed 
%      calculated by applying the mean error (ME), mean absolute 
%      error (MAE), and root mean square error (RMSE). The values 
%      cover different selected time intervals, during the ICME (blue)
%      , 2 days after the end of the ICME magnetic structure 
%      (cyan), and outside recorded ICME intervals (yellow).
%
% Reference:
%      Preconditioning of interplanetary space due to transient CME 
%      disturbances
%             
% Author:
%      Martin A. Reiss, Nov 2016.
%%
clear

load('../data/predictions.mat')
dt        = 0.25;
dateStart = '01-01-2011';                      %Select start date
dateEnd   = '31-12-2015';                      %Select end date

tPred = tESWF;                                 
vPred = vESWF;
mESWF = getintervalerror( tPred, vPred, dateStart, dateEnd, dt );

tPred = tWSA;                                 
vPred = vWSA;
mWSA = getintervalerror( tPred, vPred, dateStart, dateEnd, dt );

tPred = tRec;                                  
vPred = vRec;
mREC = getintervalerror( tPred, vPred, dateStart, dateEnd, dt );

%%
f = figure;
a1 = -15;
a2 = 150;
subplot(1,3,1)
bar(mESWF)
grid on
ylabel('Deviation [km/s]')
legend('ICME','2 days after','out','Location','northwest')
xlim([0.5,3.5])
ylim([a1,a2])
ax = gca;
ax.XTickLabel = {'ME','MAE','RMSE'};
title('ESWF')

subplot(1,3,2)
bar(mWSA)
grid on
ylabel('Deviation [km/s]')
legend('ICME','2 days after','out','Location','northwest')
xlim([0.5,3.5])
ylim([a1,a2])
ax = gca;
ax.XTickLabel = {'ME','MAE','RMSE'};
title('WSA')

subplot(1,3,3)
bar(mREC)
grid on
ylabel('Deviation [km/s]')
legend('ICME','2 days after','out','Location','northwest')
xlim([0.5,3.5])
ylim([a1,a2])
ax = gca;
ax.XTickLabel = {'ME','MAE','RMSE'};
title('Persistence (27-day)')

%%
set(f, 'Position',[0,0,1200,400])
set(gcf,'Units','inches');
screenposition = get(gcf,'Position');
set(gcf,...
    'PaperPosition',[0 0 screenposition(3:4)],...
    'PaperSize',[screenposition(3:4)]);
print -dpdf -painters ../figures/Figure2