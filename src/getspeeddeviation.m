function [ diffs ] = getspeeddeviation( tPred, vPred, dateStart, dateEnd, dt )

idx = 1;
for speedLimit=300:50:450
    load('../data/icme.mat')
    load('../data/measurements.mat')             
    tMeas = tACE;                                    
    vMeas = vACE;
    l1 = -10;
    l2 = 40;
    format    = 'dd-mm-yyyy'; 

    cond = (tCMEstart > datenum(dateStart,format)...   
        & tCMEstart < datenum(dateEnd,format));          

    tCMEstart = tCMEstart(cond);
    tCMEend = tCMEend(cond);
    tCMEfield = tCMEfield(cond);

    v_icme = v_icme(cond);
    v_max = v_max(cond);
    MC = MC(cond);
    dV = dV(cond);
    Dst = Dst(cond);
    B = B(cond);

    cond = (v_icme > speedLimit);
    tCMEstart = tCMEstart(cond);
    tCMEend = tCMEend(cond);
    tCMEfield = tCMEfield(cond);
    v_icme = v_icme(cond);
    v_max = v_max(cond);
    MC = MC(cond);
    dV = dV(cond);
    Dst = Dst(cond);
    B = B(cond);

    cond   = (tPred > datenum(dateStart,format)...   
        & tPred < datenum(dateEnd,format));         
    tPred  = tPred(cond);
    vPred  = vPred(cond);
    cond   = (tMeas > datenum(dateStart,format)...   
        & tMeas < datenum(dateEnd,format));          
    tMeas  = tMeas(cond);
    vMeas  = vMeas(cond);

    tStart = datenum(dateStart,format)+1;         
    tEnd   = datenum(dateEnd,format)-1;
    tGrid  = tStart:dt:tEnd;
    vMeas  = interp1(tMeas,vMeas,tGrid)';
    vPred  = interp1(tPred,vPred,tGrid)';
    tPred  = tGrid;
    tMeas  = tGrid;

    stop = 0;
    idxRow = 1;
    idxCol = 1;

    timeElStart = l1;
    timeElEnd = l2;
    for idxCME = 1:numel(tCMEstart)
        for idxPred=1:numel(tPred)
            if tPred(idxPred) >= tCMEstart(idxCME) && stop ==0
            stop = 1;
                for i=timeElStart:timeElEnd
                diffs(idxCol,idxRow) = vMeas(idxPred+i) - vPred(idxPred+i);
                idxRow = idxRow + 1;
                end
            end
        end
        idxRow = 1;
        idxCol = idxCol + 1;
        stop = 0;
    end
    for i=1:timeElEnd+1+(-1*timeElStart)
        vDiff(i) = mean(diffs(:,i));
        stdDiff(i) = std(diffs(:,i));
    end
    durICME(idx) = mean(tCMEend - tCMEstart);
    startField(idx) = mean(tCMEfield - tCMEstart);

    idx = idx +1;
end