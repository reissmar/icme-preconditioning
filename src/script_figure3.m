% Name: 
%      script_figure3
%
% Purpose:
%      This figure shows the speed differences (color coded) for each 
%      of the recorded ICMEs stacked by the ICME impact speed as function 
%      of time. The differences are calculated from different background 
%      solar wind models (top to bottom: ESWF, WSA, PS27). The reference 
%      time zero refers to the shock arrival of the ICME.
%
% Reference:
%      Preconditioning of interplanetary space due to transient CME 
%      disturbances
%             
% Author:
%      Martin A. Reiss, Nov 2016.
%%
clear
load('../data/predictions.mat')
load('../data/icme.mat')

dt        = 0.25;
dateStart = '01-01-2011';                        %Select start date
dateEnd   = '01-12-2015';                        %Select end date                                                 
format    = 'dd-mm-yyyy';                        

tPred = tESWF;
vPred = vESWF;
diffsESWF = getspeeddeviation( tPred, vPred, dateStart, dateEnd, dt );

load('../data/icme.mat')
cond = (tCMEstart > datenum(dateStart,format)...   
    & tCMEstart < datenum(dateEnd,format));          
v_icme = v_icme(cond);
speedLimit = 300;
cond = (v_icme > speedLimit);
v_icme = v_icme(cond);

diffsESWF = [v_icme, diffsESWF];
diffsESWF = sortrows(diffsESWF);
diffsESWF(:,1)=[];

%%
load('../data/measurements.mat')             
load('../data/predictions.mat')
load('../data/icme.mat')

tPred = tWSA;
vPred = vWSA;
diffsWSA = getspeeddeviation( tPred, vPred, dateStart, dateEnd, dt );

load('../data/icme.mat')
cond = (tCMEstart > datenum(dateStart,format)...   
    & tCMEstart < datenum(dateEnd,format));          
v_icme = v_icme(cond);
speedLimit = 300;
cond = (v_icme > speedLimit);
v_icme = v_icme(cond);

diffsWSA = [v_icme, diffsWSA];
diffsWSA = sortrows(diffsWSA);
diffsWSA(:,1)=[];

%%
load('../data/measurements.mat')              
load('../data/predictions.mat')
load('../data/icme.mat')

tPred = tRec;
vPred = vRec;
diffsREC = getspeeddeviation( tPred, vPred, dateStart, dateEnd, dt );

load('../data/icme.mat')
cond = (tCMEstart > datenum(dateStart,format)...   
    & tCMEstart < datenum(dateEnd,format));          
v_icme = v_icme(cond);
speedLimit = 300;
cond = (v_icme > speedLimit);
v_icme = v_icme(cond);

diffsREC = [v_icme, diffsREC];
diffsREC = sortrows(diffsREC);
diffsREC(:,1)=[];

%%
f = figure;
v_icme = sort(v_icme, 'descend');

subplot(3,1,1)
imagesc(diffsESWF)
clim1 = -50;
clim2 = 300;
c = colorbar;
ylabel(c,'v_{meas} - v_{mod} [km/s]','FontSize',12) 
set(gca, 'CLim', [clim1, clim2],...
    'YTick',1:15:136,'YTickLabel', v_icme(1:15:136),...
    'XTick',3:4:51,'XTickLabel', -2:1:10);
title('ESWF')
ylabel('ICME speed [km/s]')
xlabel('Time [days]')

subplot(3,1,2)
imagesc(diffsWSA)
c = colorbar;
ylabel(c,'v_{meas} - v_{mod} [km/s]','FontSize',12) 
set(gca, 'CLim', [clim1, clim2], 'YTick',1:15:136,'YTickLabel', v_icme(1:15:136),...
  'XTick',3:4:51,'XTickLabel', -2:1:10);
title('WSA')
ylabel('ICME speed [km/s]')
xlabel('Time [days]')

subplot(3,1,3)
imagesc(diffsREC)
c = colorbar;
ylabel(c,'v_{meas} - v_{mod} [km/s]','FontSize',12) 
set(gca, 'CLim', [clim1, clim2], 'YTick',1:15:136,'YTickLabel', v_icme(1:15:136),...
  'XTick',3:4:51,'XTickLabel', -2:1:10);
title('Persistence (27-day)')
ylabel('ICME speed [km/s]')
xlabel('Time [days]')

%%
set(f, 'Position',[0,0,700,900])
set(gcf,'Units','inches');
screenposition = get(gcf,'Position');
set(gcf,...
    'PaperPosition',[0 0 screenposition(3:4)],...
    'PaperSize',[screenposition(3:4)]);
print -dpdf -painters ../figures/Figure3