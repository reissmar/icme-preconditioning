idx = 1;
idx11 = 0;
for speedLimit=300:50:450

load('../data/icme.mat')
cond = (tCMEstart > datenum(dateStart,format)...    
    & tCMEstart < datenum(dateEnd,format));        

tCMEstart = tCMEstart(cond);
tCMEend   = tCMEend(cond);
tCMEfield = tCMEfield(cond);

v_icme = v_icme(cond);
v_max = v_max(cond);
MC = MC(cond);
dV = dV(cond);
Dst = Dst(cond);
B = B(cond);

cond = (v_icme > speedLimit);
tCMEstart = tCMEstart(cond);
tCMEend = tCMEend(cond);
tCMEfield = tCMEfield(cond);
v_icme = v_icme(cond);
v_max = v_max(cond);
MC = MC(cond);
dV = dV(cond);
Dst = Dst(cond);
B = B(cond);

idx11 = idx11 + 1;
numEl(idx11) = numel(v_icme);

cond   = (tPred > datenum(dateStart,format)...   %Get predicted speed for 
    & tPred < datenum(dateEnd,format));          %selected time period.
tPred  = tPred(cond);
vPred  = vPred(cond);
cond   = (tMeas > datenum(dateStart,format)...   %Get observed speed for
    & tMeas < datenum(dateEnd,format));          %selected time period.
tMeas  = tMeas(cond);
vMeas  = vMeas(cond);

tStart = datenum(dateStart,format)+1;            %Prepare 6h time grid
tEnd   = datenum(dateEnd,format)-1;
tGrid  = tStart:dt:tEnd;
vMeas  = interp1(tMeas,vMeas,tGrid)';
vPred  = interp1(tPred,vPred,tGrid)';
tPred  = tGrid;
tMeas  = tGrid;

stop = 0;
idxRow = 1;
idxCol = 1;

timeElStart = l1;
timeElEnd = l2;
for idxCME = 1:numel(tCMEstart)
    for idxPred=1:numel(tPred)
        if tPred(idxPred) >= tCMEstart(idxCME) && stop ==0
        stop = 1;
            for i=timeElStart:timeElEnd
            diffs(idxCol,idxRow) = vMeas(idxPred+i) - vPred(idxPred+i);
            idxRow = idxRow + 1;
            end
        end
    end
    idxRow = 1;
    idxCol = idxCol + 1;
    stop = 0;
end

for i=1:timeElEnd+1+(-1*timeElStart)
    length = size(diffs);
    vDiffME(i) = mean(diffs(:,i));
    vDiffMAE(i)  = sum(abs(diffs(:,i)))/length(1);
    vDiffRMSE(i) = sqrt(sum((diffs(:,i)).^2)/length(1));
    stdDiff(i) = std(diffs(:,i));
end

[errorMeasure] = geterroranalysis( vPred, vMeas );
if (measure == 1)
    vDiff = vDiffME;
	baseline = errorMeasure(1);
elseif (measure == 2)
    vDiff = vDiffMAE;
	baseline = errorMeasure(2);    
else
   	vDiff = vDiffRMSE;
    baseline = errorMeasure(3);         
end

hold on
plot(timeElStart:timeElEnd,vDiff, 'x-')
xlabel('Time [days]') 
idx = idx +1;
end

line(timeElStart:timeElEnd, ones(1,numel(timeElStart:timeElEnd))*baseline,'LineStyle',':','LineWidth',1.5)
line(ones(1,401)*0, -200:200,'LineStyle','-.','LineWidth',1.5,'Color','r')
line(ones(1,401)*0.335/dt, -200:200,'LineStyle','--','LineWidth',1.0,'Color','b')
line(ones(1,401)*1.292/dt, -200:200,'LineStyle','--','LineWidth',1.0,'Color','k')