% Name: 
%      script_figure1
%
% Purpose:
%      This figure shows the differences between measured solar wind speed 
%      (ACE/SWEPAM) and modeled solar wind speed applying ESWF 
%      (blue line), WSA (red line) and PS27 (orange line) during the 
%      years 2011-2015. The duration of each recorded ICME, as given in 
%      the Richardson and Cane catalog, is marked as vertical green bar.
%
% Reference:
%      Preconditioning of interplanetary space due to transient CME 
%      disturbances
%             
% Author:
%      Martin A. Reiss, Nov 2016.
%%
clear
dateStart = '01-01-2011';                        %Select start date
dateEnd   = '31-12-2015';                        %Select end date
format    = 'dd-mm-yyyy';                        %Specify date format

load('../data/measurements.mat')                 %Load data files
load('../data/icme.mat')
tMeas = tACE;
vMeas = vACE;

run ../src/preparedata

%%
a2 = 450;
a1 = -a2;

f = figure; 
subplot(5,1,1)
hold on 
L = numel(tCMEstart);
for idx=1:L(1)
fig0 = area([tCMEstart(idx) tCMEend(idx)], [1000 1000],...
    'FaceColor', [0.75 1 0.65], 'LineStyle','none');
area([tCMEstart(idx) tCMEend(idx)], [-1000 -1000],...
    'FaceColor', [0.75 1 0.65], 'LineStyle','none');
end
fig1 = plotprediction(tPredESWF, vMeasESWF - vPredESWF, dateStart, dateEnd);
fig2 = plotprediction(tPredWSA, vMeasWSA - vPredWSA, dateStart, dateEnd);
fig3 = plotprediction(tPredRec, vMeasRec - vPredRec, dateStart, dateEnd);
hold off
box on 

legend([fig0,fig1,fig2,fig3],'ICME','ESWF','WSA','Persistence (27-day)',...
    'Location',[0.4,0.9,0.25,0.1],'Orientation','horizontal')
datetick('x', 'mmm-yy', 'keepticks')
axis([datenum('01-01-2011', 'dd-mm-yyyy') datenum('01-01-2012', 'dd-mm-yyyy') a1 a2])
ylabel('v_{meas}-v_{mod}[km/s]')
xlabel('Date [month-year]')

subplot(5,1,2)
hold on 
L = numel(tCMEstart);
for idx=1:L(1)
fig0 = area([tCMEstart(idx) tCMEend(idx)], [1000 1000],...
    'FaceColor', [0.75 1 0.65], 'LineStyle','none');
area([tCMEstart(idx) tCMEend(idx)], [-1000 -1000],...
    'FaceColor', [0.75 1 0.65], 'LineStyle','none');
end
fig1 = plotprediction(tPredESWF, vMeasESWF - vPredESWF, dateStart, dateEnd);
fig2 = plotprediction(tPredWSA, vMeasWSA - vPredWSA, dateStart, dateEnd);
fig3 = plotprediction(tPredRec, vMeasRec - vPredRec, dateStart, dateEnd);
hold off
box on 

datetick('x', 'mmm-yy', 'keepticks')
axis([datenum('01-01-2012', 'dd-mm-yyyy') datenum('01-01-2013', 'dd-mm-yyyy') a1 a2])
ylabel('v_{meas}-v_{mod}[km/s]')
xlabel('Date [month-year]')

subplot(5,1,3)
hold on 
L = numel(tCMEstart);
for idx=1:L(1)
fig0 = area([tCMEstart(idx) tCMEend(idx)], [1000 1000],...
    'FaceColor', [0.75 1 0.65], 'LineStyle','none');
area([tCMEstart(idx) tCMEend(idx)], [-1000 -1000],...
    'FaceColor', [0.75 1 0.65], 'LineStyle','none');
end
fig1 = plotprediction(tPredESWF, vMeasESWF - vPredESWF, dateStart, dateEnd);
fig2 = plotprediction(tPredWSA, vMeasWSA - vPredWSA, dateStart, dateEnd);
fig3 = plotprediction(tPredRec, vMeasRec - vPredRec, dateStart, dateEnd);
hold off
box on 

datetick('x', 'mmm-yy', 'keepticks')
axis([datenum('01-01-2013', 'dd-mm-yyyy') datenum('01-01-2014', 'dd-mm-yyyy') a1 a2])
ylabel('v_{meas}-v_{mod}[km/s]')
xlabel('Date [month-year]')

subplot(5,1,4)
hold on 
L = numel(tCMEstart);
for idx=1:L(1)
fig0 = area([tCMEstart(idx) tCMEend(idx)], [1000 1000],...
    'FaceColor', [0.75 1 0.65], 'LineStyle','none');
area([tCMEstart(idx) tCMEend(idx)], [-1000 -1000],...
    'FaceColor', [0.75 1 0.65], 'LineStyle','none');
end
fig1 = plotprediction(tPredESWF, vMeasESWF - vPredESWF, dateStart, dateEnd);
fig2 = plotprediction(tPredWSA, vMeasWSA - vPredWSA, dateStart, dateEnd);
fig3 = plotprediction(tPredRec, vMeasRec - vPredRec, dateStart, dateEnd);
hold off
box on 

datetick('x', 'mmm-yy', 'keepticks')
axis([datenum('01-01-2014', 'dd-mm-yyyy') datenum('01-01-2015', 'dd-mm-yyyy') a1 a2])
ylabel('v_{meas}-v_{mod}[km/s]')
xlabel('Date [month-year]')

subplot(5,1,5)
hold on 
L = numel(tCMEstart);
for idx=1:L(1)
fig0 = area([tCMEstart(idx) tCMEend(idx)], [1000 1000],...
    'FaceColor', [0.75 1 0.65], 'LineStyle','none');
area([tCMEstart(idx) tCMEend(idx)], [-1000 -1000],...
    'FaceColor', [0.75 1 0.65], 'LineStyle','none');
end
fig1 = plotprediction(tPredESWF, vMeasESWF - vPredESWF, dateStart, dateEnd);
fig2 = plotprediction(tPredWSA, vMeasWSA - vPredWSA, dateStart, dateEnd);
fig3 = plotprediction(tPredRec, vMeasRec - vPredRec, dateStart, dateEnd);
hold off
box on 

datetick('x', 'mmm-yy', 'keepticks')
axis([datenum('01-01-2015','dd-mm-yyyy') datenum('01-01-2016','dd-mm-yyyy') a1 a2])
ylabel('v_{meas}-v_{mod}[km/s]')
xlabel('Date [month-year]')

%%
set(f, 'Position',[0,0,1400,980])
set(gcf,'Units','inches');
screenposition = get(gcf,'Position');
set(gcf,...
    'PaperPosition',[0 0 screenposition(3:4)],...
    'PaperSize',[screenposition(3:4)]);
print -dpdf -painters ../figures/Figure1